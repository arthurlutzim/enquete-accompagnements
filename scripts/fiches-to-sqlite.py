#!/usr/bin/env python3

# -*- encoding: utf-8 -*-

import sys
from pathlib import Path
import re
import yaml
import sqlite3

SCHEMA = """
    CREATE TABLE recensement (
      "webform_serial" INTEGER PRIMARY KEY,
      "webform_time" TEXT,
      "utilisation_des_donn_es_collect_es" INTEGER,
      "apparition_dans_l_annuaire" INTEGER,
      "notification" INTEGER,
      "nom" TEXT,
      "adresse_email" TEXT,
      "email_dans_annuaire" INTEGER,
      "en_quel_nom" TEXT,
      "nom_structure" TEXT,
      "sous_titre" TEXT,
      "nom_index" TEXT,
      "site_web" TEXT,
      "site_web_dans_annuaire" INTEGER,
      "statut_benevole" INTEGER,
      "statut_pro" INTEGER,
      "lieu_ressource" INTEGER,
      "latitude" FLOAT,
      "longitude" FLOAT,
      "ville" TEXT,
      "catalogues" TEXT,
      "commentaires_libres" TEXT
    );

    CREATE TABLE formes_juridiques (
      "forme_juridique" TEXT,
      "serial" INTEGER,
      "autre" INTGER
    );
    CREATE UNIQUE INDEX formes_juridiques_primary ON formes_juridiques ("forme_juridique", "serial");
    CREATE INDEX formes_juridiques_forme_juridique ON formes_juridiques ("forme_juridique");

    CREATE TABLE publics (
      "public" TEXT,
      "serial" INTEGER,
      "autre" INTEGER,
      "pro" INTEGER
    );
    CREATE UNIQUE INDEX publics_primary ON publics ("public", "serial");
    CREATE INDEX publics_public ON publics ("public");

    CREATE TABLE formats (
      "format" TEXT,
      "serial" INTEGER,
      "autre" INTEGER
    );
    CREATE UNIQUE INDEX formats_primary ON formats ("format", "serial");
    CREATE INDEX formats_format ON formats ("format");
    
    CREATE TABLE domaines (
      "domaine" TEXT,
      "serial" INTEGER,
      "autre" INTEGER
    );
    CREATE UNIQUE INDEX domaines_primary ON domaines ("domaine", "serial");
    CREATE INDEX domaines_domaine ON domaines ("domaine");

    CREATE TABLE zones_geo (
      "zone_geo" TEXT,
      "serial" INTEGER,
      "autre" INTEGER
    );
    CREATE UNIQUE INDEX zones_geo_primary ON zones_geo ("zone_geo", "serial");
    CREATE INDEX zones_geo_zone_geo ON zones_geo ("zone_geo");
"""

def create_db(conn):
    c = conn.cursor()
    c.executescript(SCHEMA)
    conn.commit()

def insert_query(c, table, d):
    columns = ', '.join(d.keys())
    placeholders = ', '.join([':{}'.format(k) for k in d.keys()])
    query = "INSERT INTO {} ({}) VALUES ({})".format(table, columns, placeholders)
    c.execute(query, d)

def fill_multi(c, fiche, table, field, db_field, other_field, **kwargs):
    if not fiche.get(field, None):
        return
    for value in fiche[field]:
        try:
            d = { db_field: value,
                  'serial': fiche['webform_serial'] }
            d.update(kwargs)
            insert_query(c, table, d)
        except sqlite3.IntegrityError:
            print('Oops: ({value}, {serial}) already present in {table}'.format(**d), file=sys.stderr)
    if fiche.get(other_field, None):
        d = { db_field: fiche[other_field],
              'serial': fiche['webform_serial'],
              'autre': 1 }
        d.update(kwargs)
        insert_query(c, table, d)

def process_fiche(conn, fiche):
    if fiche.get('exclure', False):
        return

    c = conn.cursor()
    fiche['statut_benevole'] = 'benevole' in fiche['statut_des_interventions']
    fiche['statut_pro'] = 'pro' in fiche['statut_des_interventions']
    if fiche.get('forme_juridique', None):
        fiche['forme_juridique'] = [fiche['forme_juridique']]
    fiche['nom_structure'] = fiche.get('nom_structure', '')
    fiche['sous_titre'] = fiche.get('sous_titre', '')

    # Move article at the end for the index
    if not 'nom_index' in fiche:
        if fiche['en_quel_nom'] == 'structure':
            m = re.match(r"^(Le|L'|L’|La|Les) (.+)$", fiche['nom_structure'], re.IGNORECASE)
            if m:
                fiche['nom_index'] = '{} ({})'.format(m.group(2).capitalize(), m.group(1).capitalize())
            else:
                fiche['nom_index'] = fiche['nom_structure']
        else:
            fiche['nom_index'] = fiche['nom']

    fiche['site_web'] = fiche.get('site_web', '')
    fiche['site_web_dans_annuaire'] = fiche.get('site_web_dans_annuaire', 0)
    fiche['lieu_ressource'] = fiche.get('lieu_ressource', 0)
    fiche['latitude'] = fiche.get('latitude', None)
    fiche['longitude'] = fiche.get('longitude', None)
    fiche['ville'] = fiche.get('ville', None)
    fiche['catalogues'] = fiche.get('catalogues', '')
    fiche['commentaires_libres'] = fiche.get('commentaires_libres', '')
    c.execute("""INSERT INTO recensement
                      VALUES (:webform_serial,
                              :webform_time,
                              :utilisation_des_donn_es_collect_es,
                              :apparition_dans_l_annuaire,
                              :notification,
                              :nom,
                              :adresse_email,
                              :email_dans_annuaire,
                              :en_quel_nom,
                              :nom_structure,
                              :sous_titre,
                              :nom_index,
                              :site_web,
                              :site_web_dans_annuaire,
                              :statut_benevole,
                              :statut_pro,
                              :lieu_ressource,
                              :latitude,
                              :longitude,
                              :ville,
                              :catalogues,
                              :commentaires_libres)
              """, fiche)
    fill_multi(c, fiche, table='formes_juridiques', field='forme_juridique', db_field='forme_juridique', other_field='autre_forme_juridique_precision')
    fill_multi(c, fiche, table='publics', field='public', db_field='public', other_field='autres_public')
    fill_multi(c, fiche, table='publics', field='public_pro', db_field='public', other_field='autres_pro', pro=1)
    fill_multi(c, fiche, table='formats', field='formats', db_field='format', other_field='autres_formats')
    fill_multi(c, fiche, table='domaines', field='domaines', db_field='domaine', other_field='autres_domaines')
    fill_multi(c, fiche, table='zones_geo', field='zones_geo', db_field='zone_geo', other_field='autres_geo')
    conn.commit()


def process_dir(in_dir_path, db_path):
    conn = sqlite3.connect(db_path)
    create_db(conn)

    for yml_path in in_dir_path.glob('*.yml'):
        print("Processing {}".format(yml_path), file=sys.stderr)
        fiche = next(yaml.safe_load_all(yml_path.open()))
        process_fiche(conn, fiche)

def main():
    if len(sys.argv) != 3:
        print("Usage: {} in_directory out.sqlite".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    in_dir_path = Path(sys.argv[1])
    db_path = Path(sys.argv[2])
    if not in_dir_path.is_dir():
        print("{} is not a directory".format(in_dir_path), file=sys.stderr)
        sys.exit(1)
    if db_path.exists():
        print("{} already exists".format(db_path), file=sys.stderr)
        sys.exit(1)
    process_dir(in_dir_path, db_path)


if __name__ == '__main__':
    main()
