// clean hyperlinks for the print
// taken from pagedjs documentation

Paged.registerHandlers(class extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    
    beforeParsed(content) {
        // first, look for all the links <a> that are referencing a link started by http or www
        const links = content.querySelectorAll('a[href^="http"], a[href^="www"]');
        // for each of those links, 
        links.forEach(link => {
            // Break after a colon or a double slash (//) or before a single slash (/), a tilde (~), a period, a comma, a hyphen, an underline (_), a question mark, a number sign, or a percent symbol.
            const content = link.textContent;
            let printableUrl = content.replace(/\/\//g, "//\u003Cwbr\u003E");
            printableUrl = printableUrl.replace(/\,/g, ",\u003Cwbr\u003E");
            // put a <wbr> element around to define where to break the line.
            printableUrl = printableUrl.replace(/(\/|\~|\-|\.|\,|\_|\?|\#|\%)/g, "\u003Cwbr\u003E$1");
            // turn hyphen in non breaking hyphen
            printableUrl = printableUrl.replace(/\-/g, "\u003Cwbr\u003E&#x2011;");
            // add a data-print-url to keep track of the previous link
            link.setAttribute("data-print-url", content);
            // modify the inner text of the link
            link.innerHTML = printableUrl;
        });
    }
});

// do not rely on CSS to add page numbers to index as it is way too slow
// with too many entries; let's do it using JavaScript once everything
// has been loaded instead
function findAncestor(node, predicate) {
    let parent = node.parentNode;
    let pageNumber = null;
    while (parent !== null) {
        if (predicate(parent)) {
            break;
        }
        parent = parent.parentElement;
    }
    return parent;
}

Paged.registerHandlers(class extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterRendered(pages) {
        let pagesArea = pages[0].pagesArea;
        pagesArea.querySelectorAll('.index .level2 a').forEach(xref => {
            // get target node
            let internalId = decodeURI(xref.href.replace(/^.*#/, '#'));
            let target = pagesArea.querySelector(internalId);
            if (target !== null) {
                let pageDiv = findAncestor(target, p => p.dataset.pageNumber !== undefined);
                if (pageDiv !== null) {
                    let pageNumber = pageDiv.dataset.pageNumber;
                    let span = xref.querySelector(".page-number");
                    if (span) {
                        span.textContent = pageNumber;
                    } else {
                        xref.innerHTML += `<span class="page-number">${pageNumber}</span>`;
                    }
                } else {
                    console.log(`Unable to find page number for “${internalId}”.`);
                }
                // add page number to index entry
            } else {
                console.log(`Unable to find “${internalId}”.`);
            }
        });
    }
});

// fix figure labels to match french conventions and be stylable
Paged.registerHandlers(class extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterRendered(pages) {
        let pagesArea = pages[0].pagesArea;
        pagesArea.querySelectorAll(".figure .caption").forEach(p => {
            p.innerHTML = p.innerHTML.replace(/Figure ([0-9]+):/, '<span class="label">Figure</span> $1 :');
        });
    }
});

// properly push last index entries to the next column instead of overflowing
// based on https://gitlab.pagedmedia.org/tools/pagedjs/issues/212#note_1912
Paged.registerHandlers(class extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    
    renderNode(node) {
        if (!(node && node.tagName && node.tagName == "LI" &&
              node.parentNode && node.parentNode.parentNode && node.parentNode.parentNode.parentNode &&
              node.parentNode.parentNode.parentNode.classList.contains("index"))) {
            return;
        }
        const nodeBottom = node.getBoundingClientRect().bottom;
        const nodeHeight = node.getBoundingClientRect().height;
        const distBottom = 0.005;

        // find the overflow line -------------------------------------------------
        const pageContent = node.closest(".pagedjs_page_content");
        const pageContentHeight = pageContent.offsetHeight;
        // in the following code line, 40% from the bottom, so 60% from the top
        const lineOverflow = (1 - distBottom) * pageContentHeight;
        // distance of the bottom node from the top of content area ---------------
        const pageContentTop = pageContent.getBoundingClientRect().top;
        const dist = nodeBottom - pageContentTop;

        // add element to push the title next page / column ----------------------------
        if (dist > lineOverflow) {
            console.log("in renderNode", "bumping", node);
            const shim = document.createElement("li");
            shim.style.height = nodeHeight + pageContentHeight - dist - 1 + "px";
            shim.classList.add("shim-li");
            shim.style.columnSpan = window.getComputedStyle(node).columnSpan;
            node.parentNode.insertBefore(shim, node);
        }
    }
});